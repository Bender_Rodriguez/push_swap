/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/31 14:18:16 by yaitalla          #+#    #+#             */
/*   Updated: 2015/08/24 18:43:14 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void			reset_small(t_tab *tab, int reset)
{
	if (bigger(tab) < smaller(tab) && tab->big < tab->small)
	{
		if (tab->big_rra == 0)
		{
			while (reset && recurcmp(tab->a[0], tab->sort[tab->maxus]) != 0)
			{
				action("rr", tab);
				reset--;
			}
		}
		while (reset)
		{
			action("rb", tab);
			reset--;
		}
	}
}

void			reset_big(t_tab *tab, int reset)
{
	if (smaller(tab) < bigger(tab))
	{
		if (tab->rra == 1)
		{
			while (reset && recurcmp(tab->a[0], tab->sort[tab->max]) != 0)
			{
				action("rrr", tab);
				reset--;
			}
		}
		while (reset)
		{
			action("rrb", tab);
			reset--;
		}
	}
	else
		action("rb", tab);
}

void			check_b(t_tab *tab)
{
	int			res;

	if (ft_atoi(tab->b[0]) < tab->sort_int[tab->mid])
	{
		if (tab->small < tab->big)
		{
			res = tab->small;
			while (res)
			{
				action("rrb", tab);
				res--;
			}
		}
		else 
		{
			res = tab->big;
			while (res)
			{
				action("rb", tab);
				res--;
			}
		}
	}
}

void			go_big(t_tab *tab)
{
	int			res;

	res = 0;
	if (tab->big_rra)
	{
		while (recurcmp(tab->a[0], tab->sort[tab->maxus]) != 0)
			action("rra", tab);
	}
	else
	{
		while (recurcmp(tab->a[0], tab->sort[tab->maxus]) != 0)
			action("ra", tab);
	}
	action("pb", tab);
	action("rb", tab);
	tab->maxus--;
	tab->big++;
	tab->bigger = 1;
}

void			go_small(t_tab *tab)
{
	if (tab->rra)
	{
		while (recurcmp(tab->a[0], tab->sort[tab->max]) != 0)
			action("rra", tab);
	}
	else
	{
		while (recurcmp(tab->a[0], tab->sort[tab->max]) != 0)
			action("ra", tab);
	}
	action("pb", tab);
	tab->max++;
	tab->small++;
	tab->bigger = 0;
}

void			choose(t_tab *tab)
{
	int			big;
	int			small;

	big = tab->big;
	small = tab->small;
	if (bigger(tab) < smaller(tab))
	{
		go_big(tab);
	}
	else
	{
		//if (tab->b[1] != NULL)
		//check_double(tab, small, tab->rra, 1);
		go_small(tab);
	}
	if (tab->moves >= 30000 && tab->moves % 30000 == 0)
		putcolor(". ", BOLD_LIGHT_GREY, 1, 0);
}
