/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file2.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/08/19 20:46:07 by yaitalla          #+#    #+#             */
/*   Updated: 2015/08/22 21:52:28 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int				is_sort(t_tab *tab)
{
	int			i;
	int			len;

	i = 0;
	len = ft_tablen(tab->sort);
	if (len == 2)
	{
		if (recurcmp(tab->a[0], tab->sort[0]) == 0)
			return (1);
		else
			return (0);
	}
	else
	{
		while (i < len)
		{
			if (recurcmp(tab->a[i], tab->sort[i]) != 0)
				return (0);
			i++;
		}
		return (1);
	}
	return (0);
}

void			check_b(t_tab *tab)
{
	int			i;
	int			piv;

	i = 0;
	putcolor("CB - ", GREEN, 1, 0);
	if (tab->b[1] != NULL)
	{
		while (tab->b[i + 1]  != NULL
				&& ft_atoi(tab->b[i]) > ft_atoi(tab->b[i + 1]))
			i++;
	}
	if (i > 1)
	{
		piv = ft_atoi(tab->b[i]);
		if (i < ft_tablen(tab->b) / 2)
		{
			while (ft_atoi(tab->b[0]) != piv)
				action("rb", tab);
		}
		else
		{
			while (ft_atoi(tab->b[0]) != piv)
				action("rrb", tab);
		}
		action("sb", tab);
	}
	putcolor("CB OK", GREEN, 1, 0);
}

void			check_swap(t_tab *tab)
{
	if (tab->b[1] != NULL && ft_atoi(tab->b[0]) < ft_atoi(tab->b[1])
			&& ft_atoi(tab->a[0]) > ft_atoi(tab->a[1]))
		action("ss", tab);
	else if (tab->b[1] != NULL && ft_atoi(tab->b[0]) < ft_atoi(tab->b[1]))
		action("sb", tab);
	else if (ft_atoi(tab->a[0]) > ft_atoi(tab->a[1]))
		action("sa", tab);
	check_b(tab);
}

void			fill_b(t_tab *tab)
{
	int			i;
//	int			rev;
//	int			j;

	i = 0;
	/*
	rev = 0;
	j = ft_tablen(tab->a) - 1;
	while (ft_atoi(tab->a[i]) > tab->sort_int[tab->mid])
		i++;
	while (ft_atoi(tab->a[j]) > tab->sort_int[tab->mid] && j >= 0)
	{
		rev++;
		j--;
	}
		*/
	if (i == 1)
		action("sa", tab);
	if (i > 1 && i < (tab->len / 2))
	{
		while (ft_atoi(tab->a[0]) > tab->sort_int[tab->mid])
		{
			action("ra", tab);
			check_b(tab);
		}
	}
	else if (i)
	{
		while (ft_atoi(tab->a[0]) > tab->sort_int[tab->mid])
		{
			action("rra", tab);
			check_b(tab);
			if (is_sort(tab))
				return ;
		}
	}
	tab->max++;
	action("pb", tab);
}

void			push_swap(t_tab *tab)
{
	int			i;

	i = tab->len / 2;
	/*
	if (tab->a[1] != NULL)
	{
		if (tab->a[0] && ft_atoi(tab->a[0]) > ft_atoi(tab->a[1]))
			action("sa", tab);
		if (is_sort(tab))
			return ;
	}
	*/
	check_swap(tab);
	while (i >= 0)
	{
		fill_b(tab);
		check_swap(tab);
		i--;
	}
	/*
	while (tab->max < (tab->len / 2))
	{
		fill_b(tab);
		if (tab->b[1] != NULL)
		{
			if (tab->b[0] && ft_atoi(tab->b[0]) < ft_atoi(tab->b[1]))
			{
				if (tab->a[0] && ft_atoi(tab->a[0]) > ft_atoi(tab->a[1]))
					action("ss", tab);
				else
					action("sb", tab);
			}
		}
	}
	*/
	/*
	if (ft_atoi(tab->a[0]) > ft_atoi(tab->a[1]))
		action("sa", tab);
	while (tab->max > 0)
	{
		action("pa", tab);
		tab->max--;
	}
	putcolor("GOOD", BOLD_DEFAULT, 1, 1);
	*/
	ft_putchar('\n');
}

void			push_swap3(t_tab *tab)
{
	if (ft_atoi(tab->a[0]) > ft_atoi(tab->a[1])
			&& ft_atoi(tab->a[1]) > ft_atoi(tab->a[2]))
	{
		action("ra", tab);
		action("sa", tab);
	}
	else if (ft_atoi(tab->a[0]) > ft_atoi(tab->a[1])
			&& ft_atoi(tab->a[1]) < ft_atoi(tab->a[2]))
	{
		if (ft_atoi(tab->a[0]) > ft_atoi(tab->a[2]))
			action("ra", tab);
		else
			action("sa", tab);
	}
	else if (ft_atoi(tab->a[0]) < ft_atoi(tab->a[1])
			&& ft_atoi(tab->a[0]) > ft_atoi(tab->a[2]))
		action("rra", tab);
	else if (ft_atoi(tab->a[0]) < ft_atoi(tab->a[2])
			&& ft_atoi(tab->a[2]) < ft_atoi(tab->a[1]))
	{
		action("sa", tab);
		action("ra", tab);
	}
}

void			mini_push_swap(t_tab *tab, int tablen)
{
	if (tablen == 2)
	{
		if (ft_atoi(tab->a[0]) > ft_atoi(tab->a[1]))
			action("sa", tab);
	}
	else if (tablen == 3)
		push_swap3(tab);
	else if (tablen == 4)
		push_swap4(tab);
	else if (tablen == 5)
		push_swap5(tab);
}
