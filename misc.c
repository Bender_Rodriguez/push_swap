/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   misc.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/08/23 21:24:52 by yaitalla          #+#    #+#             */
/*   Updated: 2015/08/24 18:44:15 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int				is_sort(t_tab *tab)
{
	int			i;
	int			len;

	i = 0;
	len = ft_tablen(tab->sort);
	if (len == 2)
	{
		if (recurcmp(tab->a[0], tab->sort[0]) == 0)
			return (1);
		else
			return (0);
	}
	else
	{
		while (i < len)
		{
			if (recurcmp(tab->a[i], tab->sort[i]) != 0)
				return (0);
			i++;
		}
		return (1);
	}
	return (0);
}

void			push_swap3(t_tab *tab)
{
	if (ft_atoi(tab->a[0]) > ft_atoi(tab->a[1])
			&& ft_atoi(tab->a[1]) > ft_atoi(tab->a[2]))
	{
		action("ra", tab);
		action("sa", tab);
	}
	else if (ft_atoi(tab->a[0]) > ft_atoi(tab->a[1])
			&& ft_atoi(tab->a[1]) < ft_atoi(tab->a[2]))
	{
		if (ft_atoi(tab->a[0]) > ft_atoi(tab->a[2]))
			action("ra", tab);
		else
			action("sa", tab);
	}
	else if (ft_atoi(tab->a[0]) < ft_atoi(tab->a[1])
			&& ft_atoi(tab->a[0]) > ft_atoi(tab->a[2]))
		action("rra", tab);
	else if (ft_atoi(tab->a[0]) < ft_atoi(tab->a[2])
			&& ft_atoi(tab->a[2]) < ft_atoi(tab->a[1]))
	{
		action("sa", tab);
		action("ra", tab);
	}
}

void			mini_push_swap(t_tab *tab, int tablen)
{
	if (tablen == 2)
	{
		if (ft_atoi(tab->a[0]) > ft_atoi(tab->a[1]))
			action("sa", tab);
	}
	else if (tablen == 3)
		push_swap3(tab);
	else if (tablen == 4)
		push_swap4(tab);
	else if (tablen == 5)
		push_swap5(tab);
}
