/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filler.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/08/23 21:26:30 by yaitalla          #+#    #+#             */
/*   Updated: 2015/09/22 18:02:25 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int				bigger(t_tab *tab)
{
	int			i;
	int			j;

	i = 0;
	j = ft_tablen(tab->a);
	while (recurcmp(tab->a[i], tab->sort[tab->maxus]) != 0)
		i++;
	if (i < (j / 2))
	{
		tab->big_rra = 0;
		return (i);
	}
	tab->big_rra = 1;
	return (j - i);
}

int				smaller(t_tab *tab)
{
	int			i;
	int			j;

	i = 0;
	j = ft_tablen(tab->a);
	while (recurcmp(tab->a[i], tab->sort[tab->max]) != 0)
		i++;
	if (i < (j / 2))
	{
		tab->rra = 0;
		return (i);
	}
	tab->rra = 1;
	return (j - i);
}

void			big_double(t_tab *tab, int pos, int way)
{
	if (way == 0)
	{
		while (pos && recurcmp(tab->a[0], tab->sort[tab->maxus]) != 0)
		{
			action("rr", tab);
			pos--;
		}
		while (pos)
		{
			action("rb", tab);
			pos--;
		}
	}
	else
	{
		while (pos)
		{
			action("rb", tab);
			pos--;
		}
	}
}

void			check_double(t_tab *tab, int pos, int way, int wich)
{
	if (wich == 1)
	{
		if (way)
		{
			while (pos && recurcmp(tab->a[0], tab->sort[tab->max]) != 0)
			{
				action("rrr", tab);
				pos--;
			}
			while (pos)
			{
				action("rrb", tab);
				pos--;
			}
		}
		else
		{
			while (pos)
			{
				action("rrb", tab);
				pos--;
			}
		}
	}
	else
		big_double(tab, pos, way);
}

void			push_swap(t_tab *tab)
{
	int			i;

	while (tab->a[0])
	{
		if (ft_atoi(tab->a[0]) > ft_atoi(tab->a[1]))
			action("sa", tab);
		choose(tab);
	}

	ft_putchar('\n');
	i = 0;
	while (recurcmp(tab->b[i], tab->sort[tab->mid]) != 0)
		i++;

	putcolor("\nmin = ", BOLD_MAGENTA, 1, 0);
	putcolor(tab->sort[0], BOLD_MAGENTA, 1, 1);

	putcolor("\ntab->b[0] = ", BOLD_BROWN, 1, 0);
	putcolor(tab->b[0], BOLD_CYAN, 1, 1);

	putcolor("\ntab->b[1] = ", BOLD_BROWN, 1, 0);
	putcolor(tab->b[1], BOLD_CYAN, 1, 1);

	putcolor("\ntab->b[", BOLD_BROWN, 1, 0);
	putcolor(ft_itoa(i), BOLD_BROWN, 1, 0);
	putcolor("]", BOLD_BROWN, 1, 0);
	putcolor(" = 0", BOLD_CYAN, 1, 1);
	
	putcolor("\ntab->b[", BOLD_BROWN, 1, 0);
	putcolor(ft_itoa(i + 1), BOLD_BROWN, 1, 0);
	putcolor("] = ", BOLD_BROWN, 1, 0);
	putcolor(tab->b[i + 1], BOLD_CYAN, 1, 1);
	
	putcolor("\ntab->b[tab->mid - 1] = ", BOLD_BROWN, 1, 0);
	putcolor(tab->b[tab->mid - 1], BOLD_CYAN, 1, 1);

	putcolor("\ntab->b[tab->mid] = ", BOLD_BROWN, 1, 0);
	putcolor(tab->b[tab->mid], BOLD_CYAN, 1, 1);

	putcolor("\ntab->b[len - 2] = ", BOLD_BROWN, 1, 0);
	putcolor(tab->b[tab->len - 2], BOLD_CYAN, 1, 1);

	putcolor("\ntab->b[len - 1] = ", BOLD_BROWN, 1, 0);
	putcolor(tab->b[tab->len - 1], BOLD_CYAN, 1, 1);

	putcolor("\nmax = ", BOLD_MAGENTA, 1, 0);
	putcolor(tab->sort[tab->len - 1], BOLD_MAGENTA, 1, 1);

	
	i = 0;
	while (recurcmp(tab->b[i], tab->sort[tab->len - 1]) != 0)
		i++;
	if (i < tab->len / 2)
	{
		while (recurcmp(tab->b[0], tab->sort[tab->len - 1]) != 0)
			action("rb", tab);
	}
	else
	{
		while (recurcmp(tab->b[0], tab->sort[tab->len - 1]) != 0)
			action("rrb", tab);
	}
	i = tab->len;
	while (i)
	{
		action("pa", tab);
		i--;
	}

	putcolor("GOOD", BOLD_DEFAULT, 1, 1);
	ft_putchar('\n');
}
